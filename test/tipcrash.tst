## Test tipceah bug, issue #437
read <<EOF
blob
mark :1
data 13
# errorrepro

reset refs/heads/main
commit refs/heads/main
mark :2
author sleichtle <sleichtle@ascona.de> 1714736581 +0200
committer sleichtle <sleichtle@ascona.de> 1714736581 +0200
data 15
Initial commit
M 100644 :1 README.md

blob
mark :3
data 0

commit refs/heads/main
mark :4
author sleichtle <sleichtle@ascona.de> 1714736624 +0200
committer sleichtle <sleichtle@ascona.de> 1714736624 +0200
data 7
commit
from :2
M 100644 :3 test.txt

blob
mark :5
data 2
x

commit refs/heads/testbranch
mark :6
author sleichtle <sleichtle@ascona.de> 1714736654 +0200
committer sleichtle <sleichtle@ascona.de> 1714736654 +0200
data 7
change
from :4
M 100644 :5 test.txt

reset refs/tags/tag
from :6

done
EOF
delete branch refs/heads/testbranch
write
list names
